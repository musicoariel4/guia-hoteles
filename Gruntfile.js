module.exports = function(grunt) {
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });
    grunt.initConfig({
				
        sass: { //task
            dist: { //target
                files: [{ //diccionario de archivos
                    expand: true,
                    cwd: './css', //dentro de la carpeta css
                    src: ['./*.scss'], //busca los file de *.scss
                    dest: './css', //destino en la carpeta css 
                    ext: '.css' //convierte o genera  *.css
                }]
            }
        },
        watch: {
            files: ['css/*.scss'],
            tasks: ['css']
        },
        browserSync: {
            dev: {
                bsFiles: { //browser files
                    src: [
                        './css/*.css',
                        '*.html',
                        './js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: "./"
                    }
                }
            }
        },
		imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: './',
                    src: ['imagen/*.{png,jpg,gif}'],
                    dest: 'dist/'
                }]
            }
        },
		copy: {
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './',
                    src: ['*.{html}'],
                    dest: 'dist'
                }]
            },
		fonts :	{
			 files: [{
                    expand: true,
                    dot: true,
                    cwd: 'node_modules.open-iconic/font',
                    src: ['font/*.*'],
                    dest: 'dist'
                }]
		    }
			
        },
		clean: {
            build: {
                src: ['dist/']
            }
        },
				
		cssmin: {
            dist: {}
        },
        uglify: {
            dist: {}
        },
		filerev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },
			release:{
               files: [{
                    src: [
                        'dist/js/*.js',
                        'dist/css/*.css',
                    ]
                }]   
			},
		},
		concat: {
			options: {
			  separator: ';',
			},
			dist: {},
		  },
		  useminPrepare: {
             foo: {
                dest: 'dist',
                src: ['index.html','medellin.html','guiaContacto.html','guiaBarranquilla.html', 'precios.html', 'guiabogota.html','guiaCartagena.html','guiaCali.html']
             },
             options: {
				 flow: {
					 css: ['cssmin'],
                        js: ['uglify']
                     },	
					 post: {
                       css: [{
                         name:'cssmin', 
						  createConfig: function (context, block) {
                            var generated = context.options.generated;
									generated.options = {
									  keepSpecialComments:0,
									  rebase:false
									}
						     }
                        }] 
                    } 
                }
			},
            usemin: {
				html: ['dist/index.html','dist/medellin.html','dist/guiaContacto.html','dist/guiaBarranquilla.html','dist/precios.html','dist/guiabogota.html', 'dist/guiaCartagena.html','dist/guiaCali.html'],
				options: {
					assetsDirs: ['dist', 'dist/css', 'dist/js']
				}
			}
            
    });	
	//grunt.loadNpmTasks('grunt-contrib-sass');
	//grunt.loadNpmTasks('grunt-contrib-watch');
	//grunt.loadNpmTasks('grunt-browser-sync');
	
    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask('img:compress', ['imagemin']);
    grunt.registerTask('build',[
	    'clean',
		'copy',
		'imagemin',
		'useminPrepare',
		'concat',
		'cssmin',
		'uglify',
		'filerev',
		'usemin'
	])
    
};